import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const storeConfig = {
  state: {
    variable: 1
  },
  mutations: {
    inc(state) {
      state.variable += 1
    },
    dec(state) {
      state.variable -= 1
    }
  },
  actions: {
    increment(context) {
      context.commit('inc')
    },
    decrement(context) {
      context.commit('dec')
    }
  },
}
export default new Vuex.Store(storeConfig)
