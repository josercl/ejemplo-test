import HelloWorld from '@/components/HelloWorld.vue'
import { createLocalVue, shallowMount } from '@vue/test-utils'
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('HelloWorld.vue', () => {
  it('muestra la variable desde vuex', () => {
    const storeConfig = {
      state: { variable: 56 },
      actions: {
        increment: jest.fn(),
        decrement: jest.fn(),
      }
    }
    const store = new Vuex.Store(storeConfig)

    const wrapper = shallowMount(HelloWorld, {
      store, localVue
    })
    const displayedText = wrapper.find('#var_val').text();
    expect(parseInt(displayedText, 10)).toEqual(storeConfig.state.variable)
  })

  it('otro test de ejemplo', () => {
    const $route = { params: { id: 4, name: 'categoryEdit' } }
    const store = new Vuex.Store({
      state: {
        list: {
          0: [],
          currentPage: 1,
          totalPages: 1,
          loading: false,
          currentItem: {}
        }
      },
      actions: {}
    })
    const localVue = createLocalVue();
    localVue.use(Vuex)
    const wrapper = shallowMount(CategoryEdit, {
      localVue,
      mocks: {
        $route
      }
    })

    expect(wrapper.whatever).toBe(otrowhatever)
  })
})
