import { createLocalVue, shallowMount } from '@vue/test-utils'
import Item from "../../src/components/Item";
import Lista from "../../src/components/Lista";

describe('Lista.vue', () => {
    let wrapper;
    const metodo = jest.fn();

    beforeEach(() => {
        wrapper = shallowMount(Lista, {
            stubs: {
                'item': Item
            },
            methods: {
                metodo
            }
        });
    });

    it('recibe el evento ItemClick', () => {
        // Aqui estas haciendo $emit
        // directamente en un componente Item
        wrapper.find(Item).vm.$emit('itemClick', 6);
        expect(metodo).toBeCalledWith(6);
    });

    it('recibe el evento ItemClick2', () => {
        // Aqui estas haciendo lo mismo pero con
        // un click de html
        wrapper.find('li:nth-child(5)').trigger('click');
        expect(metodo).toBeCalledWith(5);
    })
});
